package unimelb.mf.graphite.plugin.services;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.graphite.Metric;
import unimelb.graphite.Metrics;

public class SvcMetricsList extends PluginService {

	public static final String SERVICE_NAME = "graphite.metrics.list";
	private static final String AV_ARCHIVE_ACTOR = "arc.av.archive:http";

	public static final String[] SERVER_METRICS = new String[] {
	        "uptime", "thread", "licence", "connection", "memory",
			"os", "store", "stream", "file-descriptor", "task", "cluster", "session",
			"smb-buffers", "asset-count", "gc", "shareable", "all" };

	private Interface _defn;

	public SvcMetricsList() {
		_defn = new Interface();
		addToDefn(_defn);
	}

	static void addToDefn(Interface defn) {
		Interface.Element metric = new Interface.Element("metric", XmlDocType.DEFAULT, "Metric.", 0, Integer.MAX_VALUE);
		metric.add(new Interface.Element("path", StringType.DEFAULT, "Metric path.", 1, 1));
		metric.add(new Interface.Element("time", DateType.DEFAULT, "Metric timestamp. Defaults to current server time.",
				0, 1));
		metric.add(new Interface.Element("value", StringType.DEFAULT, "Metric value.", 0, 1));
		Interface.Element service = new Interface.Element("service", XmlDocType.DEFAULT,
				"The service to retrieve the metric value.", 0, 1);
		service.add(new Interface.Attribute("name", StringType.DEFAULT, "Service name.", 1));
		service.add(new Interface.Attribute("xpath", StringType.DEFAULT,
				"XPATH to retrieve the value from service result.", 1));
		service.setIgnoreDescendants(true);
		metric.add(service);
		defn.add(metric);

		defn.add(new Interface.Element("server-metrics", new EnumType(SERVER_METRICS),
				"Predefined Mediaflux server metrics.", 0, SERVER_METRICS.length));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "List the specified metrics (without sending to Graphite server).";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		Metrics metrics = new Metrics();
		addMetrics(executor(), args, metrics);
		for (Metric metric : metrics) {
			w.add("metric", new String[]{"time", DateTime.string(metric.timestampDate()), "path", metric.path(),
					"value", metric.value()});
		}
	}

	static void addMetrics(ServiceExecutor executor, Element args, Metrics metrics) throws Throwable {

		String metricPathPrefix = SvcMetricPathPrefixDefaultGet.getDefaultMetricPathPrefix(executor);
		if (args.elementExists("metric")) {
			List<XmlDoc.Element> mes = args.elements("metric");
			for (XmlDoc.Element me : mes) {
				PluginTask.checkIfThreadTaskAborted();
				addMetric(executor, metricPathPrefix, me, metrics);
			}
		}
		Set<String> sms = new LinkedHashSet<String>();
		if (args.elementExists("server-metrics")) {
			sms.addAll(args.values("server-metrics"));
		} else {
			if (!args.elementExists("metric")) {
				sms.add("all");
			}
		}
		if (!sms.isEmpty()) {
			addServerMetrics(executor, metricPathPrefix, sms, metrics);
		}

	}

	private static void addServerMetrics(ServiceExecutor executor, String metricPathPrefix, Set<String> sms,
			Metrics metrics) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element status = executor.execute("server.status");
		Date time = new Date();
		if (sms.contains("all") || sms.contains("uptime")) {
			long uptimeSeconds = status.longValue("uptime/@seconds");
			metrics.addMetric(metricPathPrefix + ".uptime.seconds", time, uptimeSeconds);
		}
		if (sms.contains("all") || sms.contains("thread")) {
			metrics.addMetric(metricPathPrefix + ".threads.total", time, status.value("threads/total"));
		}
		if (sms.contains("all") || sms.contains("memory")) {
			double memoryUsedMB = status.doubleValue("memory/used");
			if ("GB".equalsIgnoreCase(status.value("memory/used/@units"))) {
				memoryUsedMB = memoryUsedMB * 1000.0;
			} else if ("TB".equalsIgnoreCase(status.value("memory/used/@units"))) {
				memoryUsedMB = memoryUsedMB * 1000000.0;
			} else if ("PB".equalsIgnoreCase(status.value("memory/used/@units"))) {
				memoryUsedMB = memoryUsedMB * 1000000000.0;
			}
			metrics.addMetric(metricPathPrefix + ".memory.used.mb", time, String.format("%.3f", memoryUsedMB));
			double memoryFreeMB = status.doubleValue("memory/free");
			if ("GB".equalsIgnoreCase(status.value("memory/free/@units"))) {
				memoryFreeMB = memoryFreeMB * 1000.0;
			} else if ("TB".equalsIgnoreCase(status.value("memory/free/@units"))) {
				memoryFreeMB = memoryFreeMB * 1000000.0;
			} else if ("PB".equalsIgnoreCase(status.value("memory/free/@units"))) {
				memoryFreeMB = memoryFreeMB * 1000000000.0;
			}
			metrics.addMetric(metricPathPrefix + ".memory.free.mb", time, String.format("%.3f", memoryFreeMB));
		}
		if (sms.contains("all") || sms.contains("stream")) {
			metrics.addMetric(metricPathPrefix + ".streams.open", time, status.value("streams/nbopen"));
		}
        if (sms.contains("all") || sms.contains("file-descriptor")) {
            metrics.addMetric(metricPathPrefix + ".file.descriptor.open", time, status.value("number-open-file-descriptors"));
        }		
		if (sms.contains("all") || sms.contains("os")) {
			metrics.addMetric(metricPathPrefix + ".system.cpu.load.percent", time,
					status.value("operating-system/system-cpu-load/@pc"));
			metrics.addMetric(metricPathPrefix + ".process.cpu.load.percent", time,
					status.value("operating-system/process-cpu-load/@pc"));
			metrics.addMetric(metricPathPrefix + ".process.cpu.time.seconds", time,
					status.longValue("operating-system/process-cpu-time/@millisecs", 0) / 1000L);
			metrics.addMetric(metricPathPrefix + ".system.load.average", time,
					status.value("operating-system/system-load-average"));

		}
		if (sms.contains("all") || sms.contains("task")) {
			metrics.addMetric(metricPathPrefix + ".tasks", time, status.value("number-of-tasks"));
		}
		if (sms.contains("all") || sms.contains("licence")) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element licence = executor.execute("licence.describe").element("licence");
			Date time1 = new Date();
			int total = licence.intValue("total");
			int remaining = licence.intValue("remaining");
			metrics.addMetric(metricPathPrefix + ".licence.used", time1, total - remaining);
			metrics.addMetric(metricPathPrefix + ".licence.remaining", time1, remaining);

			// Get specific number of AV Archive, SMB and sFTP licenses held. Have to
			// iterate for this.
			// Archive users get a GENERIC licence and this actor specific WEB one
			// We are only counting the latter (so we don't count them twice).
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor.execute("licence.holder.list");
			Collection<XmlDoc.Element> holders = r.elements("holder");
			int nAV = 0;
			int nSMB = 0;
			int nSFTP = 0;
			if (holders != null) { // could be null when server restarts.

				for (XmlDoc.Element holder : holders) {
					String theActor = holder.value("@actor");
					String theApp = holder.value("app");
					if (theActor != null) {
						if (theActor.equals(AV_ARCHIVE_ACTOR)) {
							nAV++;
						}
						//
						if (theApp.equalsIgnoreCase("SFTP")) {
							nSFTP++;
						} else if (theApp.equalsIgnoreCase("SMB")) {
							nSMB++;
						}
					}
				}
			}
			metrics.addMetric(metricPathPrefix + ".licence.used.avarchive", time1, nAV);
			metrics.addMetric(metricPathPrefix + ".licence.used.sftp", time1, nSFTP);
			metrics.addMetric(metricPathPrefix + ".licence.used.smb", time1, nSMB);
		}
		if (sms.contains("all") || sms.contains("connection")) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element re = executor.execute("network.describe");
			Date time2 = new Date();
			Set<String> types = new LinkedHashSet<String>(re.values("service/@type"));
			for (String type : types) {
				List<XmlDoc.Element> ses = re.elements("service[@type='" + type + "']");
				for (XmlDoc.Element se : ses) {
					String port = se.value("@port");
					metrics.addMetric(metricPathPrefix + "." + type + "." + port + ".connections.active", time2,
							se.value("connections/active"));
					metrics.addMetric(metricPathPrefix + "." + type + "." + port + ".connections.total", time2,
							se.value("connections/total"));
				}
			}
		}

		if (sms.contains("all") || sms.contains("store")) {
			PluginTask.checkIfThreadTaskAborted();
			Collection<String> snames = executor.execute("asset.store.list").values("store/name");

			Date time3 = new Date();
			if (snames != null) {
				for (String sname : snames) {
					XmlDoc.Element se = describeStore (executor, sname);
					if (se!=null) {
						String storeType = se.value("type");
						String storeId = se.value("@id");
						String storeName = se.value("@name");
						long free = se.longValue("mount/free", 0);
						double freeGB = (double) ((double) free) / 1000000000.0;
						long size = se.longValue("mount/size", 0);
						double sizeGB = (double) ((double) size) / 1000000000.0;

						String prefix = metricPathPrefix + ".store." + storeType + "." + storeId + "_" + storeName;
						metrics.addMetric(prefix + ".free.gb", time3, String.format("%.3f", freeGB));
						String freePC = se.value("mount/free/@partition-percent");
						if (freePC != null) {
							metrics.addMetric(prefix + ".free.partition.percent", time3, freePC);
						}
						metrics.addMetric(prefix + ".size.gb", time3, String.format("%.3f", sizeGB));
						String sizePC = se.value("mount/size/@partition-percent");
						if (sizePC != null) {
							metrics.addMetric(prefix + ".size.partition.percent", time3, sizePC);
						}
					}
				}
			}
		}

		// Cluster metrics
		if (sms.contains("all") || sms.contains("cluster")) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("service", "cluster.describe");
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor.execute("system.service.exists", dm.root());
			if (r.booleanValue("exists")) {
				PluginTask.checkIfThreadTaskAborted();
				r = executor.execute("cluster.describe");
				Collection<XmlDoc.Element> nodes = r.elements("node");
				if (nodes != null) {
					for (XmlDoc.Element node : nodes) {
						String uuid = node.value("@uuid");
						boolean controller = node.booleanValue("@controller", false);
						Collection<XmlDoc.Element> capabilities = node.elements("capability");
						if (capabilities != null) {
							for (XmlDoc.Element capability : capabilities) {
								String op = capability.value("op");
								if (op.equals("io")) {
									String ioExecuted = capability.value("executed");
									metrics.addMetric(metricPathPrefix + ".cluster.node." + uuid + ".op.io.executed", time,
											ioExecuted);
								}
							}

						}
						if (!controller) {
							long nbNodeErrors = node.longValue("errors/nb-errors", 0L);
							metrics.addMetric(metricPathPrefix + ".cluster.node." + uuid + ".errors.count", time, nbNodeErrors);
						}
					}
				}
				long totalErrors = r.longValue("total-nb-errors", 0L);
				metrics.addMetric(metricPathPrefix + ".cluster.errors.count", time, totalErrors);
			}
		}

		if (sms.contains("all") || sms.contains("session")) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("size", "infinity");
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor.execute("system.session.all.describe", dm.root());
			int total = r.intValue("count");
			metrics.addMetric(metricPathPrefix + ".sessions.total", time, total);

			// Parse sesions for some protocols and some specific apps (e.g.
			// AVArchive. Hard to parse for http
			Collection<XmlDoc.Element> sessions = r.elements("session");
			int nAV = 0;
			int nSMB = 0;
			int nSFTP = 0;
			int nNFS = 0;
			for (XmlDoc.Element session : sessions) {
				String actor = session.value("actor");
				//				String app = session.value("app");
				String service = session.value("service");
				String licence = session.value("licence");
				Boolean consumed = false;
				if (licence != null) {
					// Could equally well use app
					String l = licence.toLowerCase();
					if (l.equals("smb")) {
						nSMB++;
						consumed = true;
					} else if (l.equals("sftp")) {
						nSFTP++;
						consumed = true;
					} else if (l.equals("nfs")) {
						nNFS++;
						consumed = true;
					}
				}
				if (!consumed) {
					if (actor != null) {
						String a = actor.toLowerCase();
						if (a.contains("arc.av.archive")) {
							nAV++;
						}
					} else if (service != null) {
						String s = service.toLowerCase();
						if (s.contains("arc.av.archive")) {
							nAV++;
						}
					}
				}
			}
			metrics.addMetric(metricPathPrefix + ".sessions.avarchive", time, nAV);
			metrics.addMetric(metricPathPrefix + ".sessions.sftp", time, nSFTP);
			metrics.addMetric(metricPathPrefix + ".sessions.smb", time, nSMB);
			metrics.addMetric(metricPathPrefix + ".sessions.nfs", time, nNFS);
		}

		// SMB buffers
		if (sms.contains("all") || sms.contains("smb-buffers")) {
			XmlDocMaker dm = new XmlDocMaker("args");
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor.execute("smb.server.buffer.config.describe", dm.root());
			String receive = r.value("receive-buffers/nb-active-buffers");
			String send = r.value("send-buffers/nb-active-buffers");
			metrics.addMetric(metricPathPrefix + ".smb.active-receive-buffers", time, receive);
			metrics.addMetric(metricPathPrefix + ".smb.active-send-buffers", time, send);
		}

		// Asset count
		if (sms.contains("all") || sms.contains("asset-count")) {
			XmlDocMaker dm = new XmlDocMaker("args");
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor.execute("asset.count", dm.root());
			String n = r.value("total");
			metrics.addMetric(metricPathPrefix + ".asset.count", time, n);
		}

		// server.gc.statistics
		if (sms.contains("all") || sms.contains("gc")) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor.execute("server.gc.statistics");
			List<Long> totalNbGcs = r.longValues("collector/total-nb-gc");
			long sum = 0;
			if(totalNbGcs !=null&& !totalNbGcs.isEmpty()) {
				for(int i = 0; i<totalNbGcs.size();i++) {
					long totalNbGc = totalNbGcs.get(i);
					sum += totalNbGc;
					metrics.addMetric(metricPathPrefix + ".gc.collector" + (i+1) +".total-nb-gc", time, totalNbGc);
				}
			}
			metrics.addMetric(metricPathPrefix + ".gc.total-nb-gc", time, sum);
		}
		
		// asset.shareable.processing.queue.describe
		if(sms.contains("all") || sms.contains("shareable")) {
		    PluginTask.checkIfThreadTaskAborted();
		    XmlDoc.Element r = executor.execute("asset.shareable.processing.queue.describe");
		    metrics.addMetric(metricPathPrefix+".shareable.processing-queue.size", time, r.longValue("queue/size"));
		    metrics.addMetric(metricPathPrefix+".shareable.processing-queue.active", time, r.longValue("queue/processing/nb-active"));
		    metrics.addMetric(metricPathPrefix+".shareable.processing-queue.processed", time, r.longValue("queue/processing/processed"));
		    metrics.addMetric(metricPathPrefix+".shareable.processing-queue.failures", time, r.longValue("queue/processing/nb-processing-failures"));
		}

	}

	
	// If the store is invalid and generates an exception return null
	static private XmlDoc.Element describeStore (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		try {
			XmlDoc.Element r = executor.execute("asset.store.describe", dm.root());	
			return r.element("store");
		} catch (Throwable e) {
			return null;
		}
	}


	
	private static void addMetric(ServiceExecutor executor, String metricPathPrefix, Element me, Metrics metrics)
			throws Throwable {
		while (metricPathPrefix.endsWith(".") || metricPathPrefix.endsWith(" ")) {
			metricPathPrefix = metricPathPrefix.substring(0, metricPathPrefix.length() - 1);
		}
		String path = me.value("path");
		if (!path.startsWith(metricPathPrefix + ".")) {
			while (path.startsWith(".") || path.startsWith(" ")) {
				path = path.substring(1);
			}
			path = metricPathPrefix + "." + path;
		}
		Date time = me.dateValue("time", new Date());
		String value = me.value("value");
		XmlDoc.Element service = me.element("service");
		if (value != null) {
			metrics.addMetric(path, time, value);
		} else if (service != null) {
			String serviceName = service.value("@name");
			String resultXPath = service.value("@xpath");
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add(service, false);
			PluginTask.checkIfThreadTaskAborted();
			value = executor.execute(serviceName, dm.root()).value(resultXPath);
			if (value != null) {
				time = new Date();
				metrics.addMetric(path, time, value);
			}
		} else {
			throw new IllegalArgumentException("No metric/value or metric/service.");
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}

}
